<?php

class DeployController {

	protected function deploy($required_branch = null)
	{
		// commands
		$shell_commands = array(
			'echo $PWD',
			'whoami',
			'git pull',
			'git submodule sync',
			'git submodule update',
			'git submodule status',
		);

		// force checkout desired branch
		if (! is_null($required_branch)) array_push($shell_commands, 'git checkout '.$required_branch);

		// makes sure last command is git status
		array_push($shell_commands, 'git status');

		// Run the commands for output
		$output = '';

		foreach($shell_commands AS $command)
		{
			// Run it
			$tmp = shell_exec($command);
			// Output
			$output .= '<span class="green">$</span> <span class="blue">'.$command.'</span><br/>';
			if($tmp) $output .= '<pre class="bg-black">'.htmlentities(trim($tmp)) . "</pre>";
		}

		return $output;
	}


	public function postDeploy($required_branch = null)
	{
		$output = $this->deploy($required_branch);
		echo 'Deployed successfully';
	}

	public function getDeploy($required_branch = null)
	{
		$output = $this->deploy($required_branch);
		echo $output;
	}

}

$deployment = new DeployController();

if (isset($_REQUEST['branch']))
	$deployment->getDeploy($_REQUEST['branch']);